# DISCOURSE

https://github.com/discourse/discourse

## Installation

Go to `www` folder and clone Discourse, `git clone https://github.com/discourse/discourse.git discourse`

Read on `docs/VAGRANT.md` and continue installation steps with Vagrant

Once installed, you can surf the site on `192.168.10.200:3000`

# NODEBB

https://github.com/NodeBB/NodeBB

## Installation

Go to `www` folder and clone NodeBB, `git clone https://github.com/NodeBB/NodeBB.git NodeBB`

Boot up docker container redis + NodeBB

`cd docker && docker-compose up -d`

Stop container

`cd docker && docker-compose down`

Once installed, you can surf the site on `http://localhost:4567`