#!/bin/bash

# system prep
apt-get update \
&& apt-get install -y apt-transport-https lsb-release ca-certificates wget curl \
&& wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg \
&& echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list \
&& apt-get update && apt-get -y upgrade

# tools
apt-get update && apt-get install -y vim zip unzip less wget curl apt-utils iputils-ping gnupg

# nginx
apt-get install -y nginx

# php
mkdir -p /run/php \
&& apt-get install -y php7.1 \
php7.1-fpm \
php7.1-mysql \
php7.1-dev \
php7.1-json \
php7.1-intl \
php7.1-mcrypt \
php7.1-curl \
php7.1-cli \
php7.1-xdebug \
php7.1-memcached \
php7.1-mbstring \
php7.1-zip \
php7.1-xml \
php7.1-gd

# memcached
apt-get install -y memcached

# composer
curl -sS https://getcomposer.org/installer | php \
&& mv composer.phar /usr/local/bin/composer

cp /var/docker/nginx/sites-enabled/*.conf /etc/nginx/sites-enabled \
&& cp -r /var/docker/nginx/conf-stacks /etc/nginx/conf-stacks

# install nodejs
curl -sL https://deb.nodesource.com/setup_9.x | bash - \
&& apt-get install -y nodejs

# install ruby
apt-get install -y ruby-full

# install redis
# echo -e "============== install redis ============="
# apt-get install software-properties-common \
# && echo "deb http://ftp.utexas.edu/dotdeb/ stretch all" > /etc/apt/sources.list.d/dotdeb.list \
# && echo "deb-src http://ftp.utexas.edu/dotdeb/ stretch all" >> /etc/apt/sources.list.d/dotdeb.list \
# && wget -O https://www.dotdeb.org/dotdeb.gpg \
# && apt-key add dotdeb.gpg \
# && apt-get update
# apt-get install -y redis-server
# cp -f /var/docker/redis/conf/redis.conf /etc/redis/redis.conf
# service redis-server restart
# echo -e "============== install redis ============="

service php7.1-fpm restart
service memcached restart
nginx -g "daemon off;"