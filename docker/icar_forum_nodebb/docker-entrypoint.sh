#!/bin/bash

# tools
apt-get update && apt-get install -y vim zip unzip less wget curl apt-utils iputils-ping gnupg

# install nodejs
curl -sL https://deb.nodesource.com/setup_9.x | bash -
apt-get install -y nodejs

# cd /var/www/NodeBB
./nodebb start
./nodebb log